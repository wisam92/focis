<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupFile extends Model
{
    protected $table = "group_file";
    protected $guarded = [];

}
