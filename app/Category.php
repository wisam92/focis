<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public static function Sidebar()
    {
        $cat = Category::orderBy('parent_id')->where('view_at_sidebar',1)->get();
        $json = [];
        // dd(array_key_exists( 0,$json));
        foreach($cat as $c)
        {
            if(!array_key_exists( $c->parent_id,$json) )
            {
                $t = ['name' => $c->name, "id" => $c->id, "child"=>[]];
                $json[$c->id] = $t;
            }
            else
            {
                $t =[
                    'name' => $c->name,
                    "id" => $c->id,
                    "child" => [],
                ];
                $t_old=$json[$c->parent_id]["child"];
                array_push( $t_old,$t);
                $json[$c->parent_id]["child"]=$t_old;
                // dd($json[$c->parent_id]["child"],array_key_exists( $c->parent_id,$json));
                
            }
        }
        return ($json);

        
    }
}
