<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'code' , 'role_id', 'rank_id' , 'note' ,'is_active', 'is_authorized' 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    
    public function Rank()
    {
        return $this->belongsTo('App\Rank');
    }

    public function Role()
    {
        return $this->belongsTo('App\Role');
    }

    public function Group()
    {
        return $this->belongsToMany('App\Group','user_group' ,'user_id', 'group_id' );
    }

    

    public static function generatePassword($digits = 6 )
    {
        $i = 0;
        $pin = "";
        while($i < $digits){
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }
}
