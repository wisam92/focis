<?php

namespace App\Http\Controllers;

use App\User;
use App\Group;
use App\UserGroup;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function GroupsAndUsers()
    {
        $Users = User::with('Group')->orderBy('name')->get();
        $Groups = Group::with('User')->orderBy('name')->get();
        $UserGroups = UserGroup::all();

        return Response::json([
            'Users' => $Users,
            'Groups' => $Groups,
            'UserGroups' => $UserGroups,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        // dd($request->all());
        // dd($request['checkbox']);

        // check if all user data at users DataTable was sent with out filters
        $UserGroups = UserGroup::whereNotIn('user_id', $request['users'])->get()->toArray();
        if (count($UserGroups) == 0 ){
            // ($UserGroups == 0 ) => no filters at users DataTable
            if ($request['checkbox'] != null) {
                DB::beginTransaction();
                try {
                    $DeleteUserGroups = UserGroup::truncate(); // delete all data at UserGroup table to create new UserGroup
                    foreach ($request['checkbox'] as $UserID => $GroupIDs) {
                        // dd($UserID, $GroupIDs);
                        for ($i = 0; $i < count($GroupIDs); $i++) {
                            $CreateUserGroup[] = UserGroup::create([
                                'user_id' => $UserID,
                                'group_id' => $GroupIDs[$i],
                            ]);
                        }
                    }
                    $success = true;
                    DB::commit();
                } 
                catch (\Exception $e) {
                    $success = false;
                    DB::rollback();
                    return Response::json(['status' => 'fail']);
                    // dd($e);
                }
            }
            else{
                // ($request['checkbox'] == null) => delete all UserGroup
                $DeleteUserGroups = UserGroup::truncate();
                $success = true;
            }
        }
        else {
            if ($request['checkbox'] != null) {
                DB::beginTransaction();
                try {
                    for ($i = 0; $i < count($request['users']); $i++) {
                        // delete just $request['users'] (users have been filtered) at UserGroup table to create new UserGroup
                        $DeleteUserGroups = UserGroup::where('user_id', $request['users'][$i])->delete(); 
                    }
                    foreach ($request['checkbox'] as $UserID => $GroupIDs) {
                        // dd($UserID, $GroupIDs);
                        for ($i = 0; $i < count($GroupIDs); $i++) {
                            $CreateUserGroup[] = UserGroup::create([
                                'user_id' => $UserID,
                                'group_id' => $GroupIDs[$i],
                            ]);
                        }
                    }
                    $success = true;
                    DB::commit();
                } 
                catch (\Exception $e) {
                    $success = false;
                    DB::rollback();
                    return Response::json(['status' => 'fail']);
                    // dd($e);
                }
            }
            else{
                // ($request['checkbox'] == null) => delete $request['users'] UserGroup
                for ($i = 0; $i < count($request['users']); $i++) {
                    // delete just $request['users'] (users have been filtered) at UserGroup table
                    $DeleteUserGroups = UserGroup::where('user_id', $request['users'][$i])->delete(); 
                }
                $success = true;
            }
            // dd($request->all() , $UserGroups);
        }

        if ($success) {
            return Response::json(['status' => 'success']);
        }
        return Response::json(['status' => 'fail']);
    }
    
    public function store1(Request $request)
    {
        // dd($request->all());
        // dd($request['checkbox']);
        if ($request['checkbox'] != null) {
            foreach ($request['checkbox'] as $UserID => $GroupIDs) {
                dd($UserID, $GroupIDs);
                $UserGroups = UserGroup::where('user_id', $UserID)->get();
                if ($UserGroups) {
                    DB::beginTransaction();
                    try {
                        // dd($UserGroups);
                        // dd($GroupIDs, $UserID);
                        // $DeleteUserGroups = UserGroup::where('user_id', $UserID)->whereIn('group_id',$GroupIDs)->delete();
                        // for ($i = 0; $i < count($GroupIDs); $i++){
                        // $DeleteUserGroups = UserGroup::where('user_id', $UserID)->delete();
                        $DeleteUserGroups = UserGroup::truncate();
                        // }
                        // dd($DeleteUserGroups);
                        
                        // $ShowUserGroups = UserGroup::where('user_id', $UserID)->get();
                        // dd($ShowUserGroups);
                        // dd($UserGroups);
                        for ($i = 0; $i < count($GroupIDs); $i++) {
                            $CreateUserGroup[] = UserGroup::create([
                                'user_id' => $UserID,
                                'group_id' => $GroupIDs[$i],
                            ]);
                        }

                        // dd($CreateUserGroup);
                        $success = true;
                        DB::commit();
                    } catch (\Exception $e) {
                        $success = false;
                        DB::rollback();
                        dd($e);
                    }
                }
            }
        }

        if ($success) {
            return Response::json(['status' => 'success']);
        }
        return Response::json(['status' => 'fail']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserGroup  $userGroup
     * @return \Illuminate\Http\Response
     */
    public function show(UserGroup $userGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserGroup  $userGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(UserGroup $userGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserGroup  $userGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserGroup $userGroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserGroup  $userGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserGroup $userGroup)
    {
        //
    }
}
