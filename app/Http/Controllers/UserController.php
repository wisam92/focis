<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Rank;
use Illuminate\Http\Request;
use DataTables;
use Response;
use Auth;
use Hash;
use Group;   
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Image;
use Excel;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Rank = Rank::all();
        $Role = Role::all();
        if (auth::user()->role_id == 1){
            return view('user', compact('Rank', 'Role'));
        }
        else {
            return abort('404');
        }
        
    }

    public function GetUsers()
    {
        // $from = $request['data']['order_from_date'];
        // $to = $request['data']['order_to_date'];
        // $title = $request['data']['order_title_search'];
        // $title = $title == null ? '%' : $title;

        $User = User::with('Role')->with('Rank')
        ->orderBy('id','desc')->get();

        return Datatables::of($User)->make(true);
        // return response()->json(['data' => $User]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pass = User::generatePassword();
        // dd($request->all());
        $User = User::create([
            'is_active' => 1,
            'code' => $request['new_code'],
            'name' => $request['new_name'],
            'email' => $request['new_email'],
            'password' => Hash::make( $pass),
            // 'password' =>  Hash::make('102030'),
            'note' => $request['new_note'],
            'role_id' => $request['new_role_id'],
            'rank_id' => $request['new_rank_id'],
            
        ]);
        
        if ($User)
        {
            $cc = '';
            $bcc = 'cep@chamwings.com';
            MailController::SendEmailToNewUser( $request['new_code'], $pass, $request['new_email'], $cc, $bcc );
            return Response::json(['status' => 'success']);
        }
        else 
            return Response::json(['status' => 'fail']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role, $id)
    {
        $User = User::with('Group')->with('Role')->with('Rank')->find($id);
        if ($User != null ){
            return response()->json(['status' => 'success' , 'user' => $User]);
        }
        else 
            return response()->json(['status' => 'fail' ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $User = User::find($request['edit_user_id']);
        // dd($User);
        $User = $User->update([
            'code' => $request['edit_code'],
            'name' => $request['edit_name'],
            'email' => $request['edit_email'],
            'note' => $request['edit_note'],
            'role_id' => $request['edit_role_id'],
            'rank_id' => $request['edit_rank_id'],
        ]);

        
        

        if ($User)
            return Response::json(['status' => 'success']);
        else 
            return Response::json(['status' => 'fail']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }

    // public function ResetPassword(Request $request)
    // {
    //     // dd($request->all());

    //     // $request->validate([
    //     //     'new_password' => 'required|string|min:6|confirmed',
    //     // ]);

    //     $pass = User::generatePassword();

    //     if ($request['new_password'] == $request['new_password_confirmation'])
    //     {
    //         $User = User::find($request['pass_user_id']);
    //         $UserName = $User->name;
    //         $UserMail = $User->email;
    //         $UserCode = $User->code;
    //         $UserPassword = Hash::make( $pass);
            
    //         // dd($)
    //         $User = $User->update([
    //             // 'password' => Hash::make(bcrypt($request['new_password'])),
    //             'password' => (bcrypt($UserPassword)),
    //          ]);
    //         if ($User)
    //         {
    //             $cc = "";
    //             $bcc = "cep@chamwings.com";
    //             $body = '
    //                     Dear <b> ' . $UserName . '</b> <br><br>
    //                     Please note that your <b>FOCIS</b> account password has been reset recently, find out it below: <br>
    //                     <u> URL:</u> <a href= "https://focis.chamwings.com">https://focis.chamwings.com</a>
    //                     <br>
    //                     Username: <b> ' . $UserMail . ' </b> <u>OR</u> <b>"'. $UserCode .'"</b> Code <br>
    //                     Password: <b> ' . $UserPassword . ' </b><br>
    //                     ';
    //             MailController::Mail( 'FOCIS | Change Password', 'Change Password', $UserMail, $cc, $bcc, $body);
    //             return Response::json(['status' => 'success' , 'message' => 'ok']);
    //         }
               
    //         else 
    //             return Response::json(['status' => 'fail' , 'message' => 'Something went wrong please try again']);
    //     }
    //     else 
    //     {
    //         return Response::json(['status' => 'fail' , 'message' => 'The password confirmation does not match']);
    //     }
        

    // }
    public function ResetPassword1(Request $request)
    {
        $pass = User::generatePassword();
         return $this->ResetPassword($pass , $request['pass_user_id']);
    }

    public function ResetPassword2(Request $request)
    {
        // dd($request->all());
        if ($request['new2_password'] ==  $request['new2_password_confirmation']){
            return $this->ResetPassword($request['new2_password'] , $request['pass2_user_id']);
        }
        else {
            return Response::json(['status' => 'fail' , 'message' => 'The password confirmation does not match']);
        }
        
    }

    public function ResetPassword($pass, $user_id)
    {
        // dd($pass, $user_id);
        $User = User::find($user_id);
        $UserName = $User->name;
        $UserMail = $User->email;
        $UserCode = $User->code;
        Log::info([
            'UserName' => $UserName,
            'Password' => $pass,
        ]);
        
        // dd($)
        $User = $User->update([
            // 'password' => Hash::make(bcrypt($request['new_password'])),
            'password' => Hash::make( $pass),
            ]);
        if ($User)
        {
            $cc = "";
            $bcc = "cep@chamwings.com";
            $body = '
                    Dear <b> ' . $UserName . '</b> <br><br>
                    Please note that your <b>FOCIS</b> account password has been reset recently, find out it below: <br>
                    <u> URL:</u> <a href= "https://focis.chamwings.com">https://focis.chamwings.com</a>
                    <br>
                    Username: <b> ' . $UserMail . ' </b> <u>OR</u> <b>"'. $UserCode .'"</b> Code <br>
                    Password: <b> ' . $pass . ' </b><br>
                    ';
            MailController::Mail( 'FOCIS | Change Password', 'Change Password', $UserMail, $cc, $bcc, $body);
            return Response::json(['status' => 'success' , 'message' => 'Great, New password has been reset and sent to '. $UserName .'  successfully']);
        }
            
        else 
            return Response::json(['status' => 'fail' , 'message' => 'Something went wrong please try again']);

    }

    public function Active(Request $request)
    {
        // dd($request->all());
        $User = User::find($request['active_user_id']);
        // dd($User->is_active);
            $User = $User->update([ 
                'is_active' => $request['is_active'],
                'is_authorized' => $request['is_Authorized'],
            ]);
      
            if ($User)
            return Response::json(['status' => 'success' ]);
        else 
            return Response::json(['status' => 'fail']);

    }

    public function SendWelcomingEmailToAllUsers(Request $request)
    {
        $Users = User::all();
        // dd($User->toArray());
        $start = microtime(true);
        Log::info('Start Send Welcoming Emails, Total Count is: ' . count($Users));
        $IDs = [];
        $Log = [];
        $count = 0;
        $cc = "";
        $bcc = "";
        foreach ($Users as $user) {
            $pass = User::generatePassword();
            $UserID = $user->id;
            $UserName = $user->name;
            $UserMail = $user->email;
            $UserCode = $user->code;

            $User1 = User::find($UserID);
            $User1 = $User1->update([
                'password' => (bcrypt($pass)),
             ]);
            if ($User1)
            {
                $Mail = MailController::SendEmailToNewEmployee($UserCode, $pass, $UserMail, $cc, $bcc );
                $Mail = 1;
            }
            if ($Mail) {
                $IDs[] = $UserID;
                $Log['Name'] = $UserName;
                $Log['SendEmail'] = 'Yes';
            } else {
                $Log['Name'] = $UserName;
                $Log['SendEmail'] = 'No';
            }
            Log::info($Log);

            $count++;

            if ($count % 20  == 0) {
                // sleep for 3 seconds
                sleep(3);
            }
        }
        Log::info('End Send Welcoming Emails, Total Email Sends is: ' . count($IDs));

        $title = "Send Welcoming Email To All FOCIS Users";
        $subject = "FOCIS Users";
        $to = 'wsamman@chamwings.com, Mbagdadi@chamwings.com, mdallal@chamwings.com';
        // $cc = "Sgalides@chamwings.com";
        $date = date("F Y");
        $body = 'Kindly be informed that all <b> FOCIS </b> users has just been issued at <b>' . $date . '</b> , 
        please check the log file
        <br><br> Best Regards';
        $Mail = MailController::Mail( $title, $subject, $to, $cc, $bcc, $body);

        return response::json(['status' => 200]);
    }
}
