<?php

namespace App\Http\Controllers;

use App\Rank;
use Illuminate\Http\Request;
use DataTables;
use Response;

class RankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd($request->all());
        $Rank = Rank::create([
            'name' => $request['new_r_name'],
        ]);

        if ($Rank)
        {
            return Response::json(['status' => 'success']);
        }
        else 
            return Response::json(['status' => 'fail']);
    }

    public function GetRank()
    {
        $Rank = Rank::all();
        return Datatables::of($Rank)->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Rank = Rank::create([
            'name' => $request['new_r_name'],
        ]);

        if ($Rank)
        {
            return Response::json(['status' => 'success']);
        }
        else 
            return Response::json(['status' => 'fail']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rank  $rank
     * @return \Illuminate\Http\Response
     */
    public function show(Rank $rank , $id)
    {
        $Rank = Rank::find($id);
        if ($Rank != null ){
            return response()->json(['status' => 'success' , 'Rank' => $Rank]);
        }
        else 
            return response()->json(['status' => 'fail' ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rank  $rank
     * @return \Illuminate\Http\Response
     */
    public function edit(Rank $rank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rank  $rank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rank $rank)
    {
        $Rank = Rank::find($request['edit_r_id']);
        // dd($Rank);
        $Rank = $Rank->update([
            'name' => $request['edit_r_name'],
        ]);

        if ($Rank)
            return Response::json(['status' => 'success']);
        else 
            return Response::json(['status' => 'fail']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rank  $rank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // dd( $request->all());
        $Rank = Rank::destroy($request['deleteID']);
        // dd($Rank);
        if (!$Rank){
            return Response::json(['status' => 'fail']);
        }
        else{
            return Response::json(['status' => 'success']);
        }
    }
}
