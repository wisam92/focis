<?php

namespace App;
use App\Category;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    protected $guarded = [];

    public function Category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function Group()
    {
        return $this->belongsToMany('App\Group','group_file' ,'file_id', 'group_id' );
    }

    public function GroupFile(){
        return $this->belongsTo('App\Category', 'category_id');
    }

    

}
