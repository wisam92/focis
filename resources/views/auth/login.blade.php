@extends('master.app')
@section('title' , 'Login')

@section('content')
 <script src="/fullscreen.js"></script> 
  <link href="https://swisnl.github.io/jQuery-contextMenu/dist/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://swisnl.github.io/jQuery-contextMenu/dist/jquery.contextMenu.js" type="text/javascript"></script>
  <script src="https://swisnl.github.io/jQuery-contextMenu/dist/jquery.ui.position.min.js" type="text/javascript"></script>
  <script src="https://swisnl.github.io/jQuery-contextMenu/js/main.js" type="text/javascript"></script>
  <style>
    body{
      overflow: auto;
      background-attachment: fixed; 
      background-color: #ffffff;
      background-image: url('/img/bg.jpg');

   
      background-repeat: no-repeat;
      background-size: cover;
      background-position: center;
      background-attachment: fixed;

    }

    .item-selected{
      background-color: aliceblue;
    }
  
  </style>

<main class="mt-5 pt-5">
  <div class="container">
    <section class="dark-grey-text">
      <div class="row pr-lg-5">
        <div class="col-md-5 align-items-center">

          <!--<div class="card z-depth-2" style="background-color: #ffffff60;border-radius:3%;">-->
          <!--  <div class="card-body" >-->
              <div>
                {{--  style="margin-left: 25%;margin-right: 25%;"  --}}
                <img src="/img/fo_title.png" style="margin-left:25px" width="70%" class="img-fluid mb-4"  />
                {{--  <h5 class="font-weight-bold  pt-4">Flight Operations Information System</h5>  --}}
                <br>
                <form method="POST" action="{{ route('login') }}">
                  @csrf
                    <div class="md-form form-sm ">
                      <i class="fas fa-envelope prefix"></i>
                      <input id="email" type="text" class="form-control form-control-sm  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                      <label for="form3">Your email</label>
                        @error('email')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                      
                    </div>
        
                    <div class="md-form form-sm">
                      <i class="fas fa-lock prefix"></i>
                      <input id="password" type="password" class="form-control  form-control-sm @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                      <label for="form3">Your password</label>
                        @error('password')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                          
                        @enderror
    
    
                      
                          @error('username')
    
                            <span class="invalid-feedback" role="alert">
    
                                <strong>{{ $message }}</strong>
    
                            </span>
    
                        @enderror
    
                    </div>
                    <div class="text-center mt-4">
                      <button class="btn btn-primary waves-effect waves-light">Log in
                        <i class="fas fa-sign-in ml-1"></i>
                      </button>
                    </div>
                </form>
                
      
              </div>
          <!--  </div>-->
          <!--</div>-->




          
        </div>

        <div class="col-md-7 mb-4">
  
          <div class="view">
            {{--  <img src="/img/aircratf.png" class="img-fluid" alt="smaple image">  --}}
          </div>
  
        </div>
      </div>
  
    </section>
  </div>
 
</main>

  <script>
    $(document).ready(function() {
      
     
    });


  </script>

@endsection