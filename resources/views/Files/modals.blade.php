<div class="modal fade" id="FilePerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  data-keyboard="false" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form id ="FilePermissionsForm">
        <div class="modal-content modal-fluid">
          <div class=" md-tabs tabs-2 light-blue darken-3">  <span class="text-white"> <i class="fas fa-cogs mr-1"></i> Edit File Permission </span> </div>
          <div class="card-body">
            <div class="form-group" style="height: 10px;">
              <h5 style="display: none; height: 100px;" class="FilePermissionsLoader "><i
                  class="fas fa-spinner fa-lg1 fa-spin fa-fw"></i>
                Please Wait .. </h5>
            </div>
            <input type="hidden" name="id" id="PermissionFileID">
            <div id="FileGroupsDiv"></div>
            <div class="pull-right">
              <button type="submit" id="FilePermissionsFormbtn" class="btn btn-primary btn-sm">Save Changes</button>
              <button type="button" class="btn btn-secondary btn-sm btn-block1 my-4" data-dismiss="modal">Close</button>  
            </div>
          </div>
        </div>
        </form>
    
    </div>
</div>

<div class="modal fade" id="UploadFileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  data-keyboard="false" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-fluid">
            <div class=" md-tabs tabs-2 light-blue darken-3">  <span class="text-white"> <i class="fas fa-upload mr-1"></i> Upload File </span> </div>
            <div class="card-body">
            <p class="card-text">File must be less than 200 megabytes.  Only <code>  PDF, JPG, JPEG, GIF, SVG and PNG </code> types are accepted.</p>
                <div class="row">
                    <div class="col-md-12">
                        <div class="dropzone" id="my-dropzone" name="mainFileUploader">
                            <div class="fallback">
                                <input name="file" type="file" multiple />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                    {{-- <input type="hidden" name="new_parent" id="Upload_parent"> --}}
                    <div class="pull-right">
                        <button class="btn btn-danger btn-sm btn-block1 my-4" id="RemoveAllFiles" type="button">Clear the Screen </button>
                        <button class="btn btn-info btn-sm btn-block1 my-4" id="submit" type="submit">Upolad Files </button>
                        <button type="button" class="btn btn-secondary btn-sm btn-block1 my-4" data-dismiss="modal">Close</button>  
                    </div>
                    
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="NewFolderModal" tabindex="-1" role="dialog"  data-keyboard="false" data-backdrop="static" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class=" md-tabs tabs-2 light-blue darken-3">  <span class="text-white"> <i class="fas fa-folder-open mr-1"></i> New Folder </span> </div>
        <form action="" method="POST" id="NewFolderForm" >
            @csrf
            <div class="modal-body mb-3">
            <div class="md-form form-sm">
                <input type="text" id="new_name" name="new_name" class="form-control form-control-sm">
                <label for="new_name">Folder Name </label>
            </div>

            <input type="hidden" name="new_parent" id="new_parent">

                <button class="btn btn-info btn-sm btn-block1 my-4" type="submit">submit </button>
                <button type="button" class="btn btn-secondary btn-sm btn-block1 my-4" data-dismiss="modal">Close</button>  


            </div>
        </form>
        
        </div>
    </div>
</div>

<div class="modal fade" id="EditFolderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  data-keyboard="false" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class=" md-tabs tabs-2 light-blue darken-3">  <span class="text-white"> <i class="fas fa-edit mr-1"></i> Rename Folder </span> </div>
        <form action="" method="POST" id="EditFolderForm" >
            @csrf
            <div class="modal-body mb-3">
            <div class="md-form form-sm">

                <input type="text" id="edit_name" name="edit_name" class="form-control form-control-sm">
                <label >Folder Name </label>
                <input type="hidden" id="edit_catID" name="edit_catID">
            </div>

                <button class="btn btn-info btn-sm btn-block1 my-4" type="submit">Update </button>
                <button type="button" class="btn btn-secondary btn-sm btn-block1 my-4" data-dismiss="modal">Close</button>  


            </div>
        </form>
        
        </div>
    </div>
</div>

<div class="modal fade" id="EditFileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  data-keyboard="false" data-backdrop="static"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class=" md-tabs tabs-2 light-blue darken-3">  <span class="text-white"> <i class="fas fa-edit mr-1"></i> Rename File </span> </div>
        <form action="" method="POST" id="EditFileForm" >
            @csrf
            <div class="modal-body mb-3">
            <div class="md-form form-sm">
                <label >File Name </label>
                <br>
                <input type="text" id="edit_fileName" name="edit_fileName" class="form-control form-control-sm">
                <input type="hidden" id="edit_fileID" name="edit_fileID">
                <input type="hidden" id="deleteType" name="deleteType">

                <button class="btn btn-info btn-sm btn-block1 my-4" type="submit">Update </button>
                <button type="button" class="btn btn-secondary btn-sm btn-block1 my-4" data-dismiss="modal">Close</button>  


            </div>
        </form>
        
        </div>
    </div>
</div>
</div>

    <style>
        body{
            overflow-x: hidden;
        }
    </style>