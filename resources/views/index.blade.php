@extends('master.app')
@section('title' , ':: Home - Flight Operations ::')

@section('content')
@include('master.header')

{{--  <!-- Modal -->
  <div class="modal fade" id="NewFolderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class=" md-tabs tabs-2 light-blue darken-3">  <span class="text-white"> <i class="fas fa-user mr-1"></i> Login </span> </div>
        <form method="POST" id="NewFolderForm" >
          @csrf
          <div class="modal-body mb-1">
            <div class="md-form form-sm">
              <i class="fas fa-envelope prefix"></i>
              <input type="text" id="form2" class="form-control form-control-sm">
              <label for="form2">Your email</label>
            </div>

            <div class="md-form form-sm">
              <i class="fas fa-lock prefix"></i>
              <input type="password" id="form3" class="form-control form-control-sm">
              <label for="form3">Your password</label>
            </div>
            <div class="text-center mt-4">
              <button class="btn btn-primary waves-effect waves-light">Log in
                <i class="fas fa-sign-in ml-1"></i>
              </button>
            </div>
          </div>
        </form>
      
      </div>
    </div>
  </div> 
--}}

<main class="mt-5 pt-5">
  <div class="container1 ">
    <div class="row">
      <div class="col-md-2">

        @include('include.sidebar')
      </div>

      <div class="col-md-10">
        <h4 class="text-center font-weight-bold mt-3 mb-2"> <span class="ml-1 badge badge-danger">New</span>
          <br><span class="text-center small text-secondary">Last Week </h4>
        </h4>
        <div class="LatestFiles"></div>

        <hr class="mt-5">
        <div class="mt-2 pt-4"></div>
        <h4 class="text-center font-weight-bold text-danger mb-2"> Reminders </h4>
        <div class="Reminders"></div>

        {{--  <hr>
        <div class="mt-4 pt-4"></div>
        <h4 class="text-center font-weight-bold mb-2"> Manuals & Forms <span class="ml-1 badge badge-info">All</span> </h4> 
        <div class="AllFilesForReading"></div>  --}}

      </div>

    </div>

  </div>
</main>

<script>
  var Gloudal_CategoryType = 'list'; 
    var CurrentPath = 0; 
    $(document).ready(function() {
       
        {{--  $('#sidebar_tree ul').mdbTreeview()  --}}
      LatestFiles('list')
      Reminders('list');
      {{--  AllFilesForReading('list');  --}}

    });

    function LatestFiles(CategoryType)
    {
      $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '/LatestFiles',
        data: {'type' : CategoryType },
        type: 'post',
        success: function (data) {
          $('.LatestFiles').html(data.html);
        },
        complete: function(){
        }
      
      });
    }

    function Reminders(CategoryType)
    {
      // console.log(CurrentPath);
      $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '/Reminders',
        data: {'type' : CategoryType },
        type: 'post',
        success: function (data) {
          $('.Reminders').html(data.html);
        },
        complete: function(){
        }
      
      });
    }

    function AllFilesForReading(CategoryType)
    {
      // console.log(CurrentPath);
      $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '/AllFilesForReading',
        data: {'type' : CategoryType },
        type: 'post',
        success: function (data) {
          $('.AllFilesForReading').html(data.html);
        },
        complete: function(){
        }
      
      });
    }

</script>
@endsection