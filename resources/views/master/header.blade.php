  <style>
    .text-dark{
      {{--  background-color: #313335;  --}}
    }

    .text-red{
    color: #f37570;
    }

    .center{
      text-align: center;
    }
    .padding-rigth{
      padding-left: 10px;
    }

    .m-b-md {
      margin-bottom: 30px;
    }
    .nav-link{
      font-size: 16px;
    }
  </style>
<header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar" >
      <div class="container">

        <!-- Brand -->
        <a  class="center navbar-brand waves-effect"  href="/" >
          <img src="/img/logo.jpg" width="250px" class="float-left img-fluid " />
          {{--  <br>  --}}
          {{--  <span class="small "> Flight Operations Information System</span>  --}}
        </a>
        <span class="padding-rigth"></span>

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="navbar-nav mr-auto">
            <li class="nav-item text-red ">
              <a class="nav-link waves-effect" href="/">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item text-red">
              <a class="nav-link waves-effect" href="/Manuals">Manuals & Forms  </a>
            </li>
            @if (Auth::user()->role_id == 1)
              <li class="nav-item text-red">
                <a class="nav-link waves-effect" href="/Reports">Reports </a>
              </li>

              <li class="nav-item text-red">
                <a class="nav-link waves-effect" href="/Settings">Settings </a>
              </li>
            
             
            @endif
          </ul>

          <!-- Right -->
          <ul class="navbar-nav nav-flex-icons">
            @guest
              <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
              </li>
             
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        {{-- <span class="nav-link pointer " onclick="ResetPassword({{ Auth::user()->id }})" >Reset Password</span> --}}

                        <a class="dropdown-item"
                          onclick="ResetPassword2({{ Auth::user()->id }})"> Reset Password
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>

                     <li class="nav-item">
                
              </li>
                </li>
            @endguest
        
          </ul>

          

        </div>


        
      </div>
    </nav>
    <!-- Navbar -->

  </header>

  