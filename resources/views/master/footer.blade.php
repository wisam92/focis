    {{--  <footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">

       

        <hr class="my-4">

        <!-- Social icons -->
        <div class="pb-4">
        <a href="https://www.facebook.com/mdbootstrap" target="_blank">
            <i class="fab fa-facebook-f mr-3"></i>
        </a>

        <a href="https://twitter.com/MDBootstrap" target="_blank">
            <i class="fab fa-twitter mr-3"></i>
        </a>

        <a href="https://www.youtube.com/watch?v=7MUISDJ5ZZ4" target="_blank">
            <i class="fab fa-youtube mr-3"></i>
        </a>

        <a href="https://plus.google.com/u/0/b/107863090883699620484" target="_blank">
            <i class="fab fa-google-plus-g mr-3"></i>
        </a>

        <a href="https://dribbble.com/mdbootstrap" target="_blank">
            <i class="fab fa-dribbble mr-3"></i>
        </a>

        <a href="https://pinterest.com/mdbootstrap" target="_blank">
            <i class="fab fa-pinterest mr-3"></i>
        </a>

        <a href="https://github.com/mdbootstrap/bootstrap-material-design" target="_blank">
            <i class="fab fa-github mr-3"></i>
        </a>

        <a href="http://codepen.io/mdbootstrap/" target="_blank">
            <i class="fab fa-codepen mr-3"></i>
        </a>
        </div>
        <!-- Social icons -->

        <!--Copyright-->
        <div class="footer-copyright py-3">
        © 2019 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/" target="_blank"> MDBootstrap.com </a>
        </div>
        <!--/.Copyright-->

    </footer>  --}}

  <!-- SCRIPTS -->
  <!-- JQuery -->
  {{-- <script type="text/javascript" src="{{ asset('MDB/js/jquery-3.4.1.min.js') }}"></script> --}}
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="{{ asset('MDB/js/popper.min.js') }}"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="{{ asset('MDB/js/bootstrap.min.js') }}"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="{{ asset('MDB/js/mdb.min.js') }}"></script>

  
  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
  {{--  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/sl-1.2.4/datatables.min.js"></script>
  <script src="https://cdn.datatables.net/plug-ins/1.10.16/api/row().show().js"></script>  --}}

  
  <!-- Initializations -->
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();

    $('#ResetPasswordForm').on('submit', function(e){
      $('#pass_user_message').html('');
        e.preventDefault();
        var data = $(this).serializeArray();
        $.ajax({
            url: '/ResetPassword',
            data: data,
            type: 'post',
            success: function(data) {
                // console.log(data.status);
              if (data.status == 'success') {
                $('#pass_user_message').html('<span class="text-success">'+ data.message +'</span>');
              } else if (data.status == 'fail') 
              {
                $('#pass_user_message').html('<span class="text-danger">'+ data.message +'</span>');
              }
            },
            complete: function() {},
        })
    });

    $('#ResetPasswordForm2').on('submit', function(e){
      $('#pass2_user_message').html('');
        e.preventDefault();
        var data = $(this).serializeArray();
        $.ajax({
            url: '/ResetPassword2',
            data: data,
            type: 'post',
            success: function(data) {
                // console.log(data.status);
              if (data.status == 'success') {
                $('#pass2_user_message').html('<span class="text-success">'+ data.message +'</span>');
              } else if (data.status == 'fail') 
              {
                $('#pass2_user_message').html('<span class="text-danger">'+ data.message +'</span>');
              }
            },
            complete: function() {},
        })
    });

  </script>