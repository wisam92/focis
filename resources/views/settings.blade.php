@extends('master.app')
@section('title' , ':: Settings - Flight Operations ::')

@section('content')
@include('master.header')
<style>
  select {
    border: 0px !important;
    border-bottom: 1px solid #ced4da !important;
  }

  select:focus:not([readonly]) {
    border: 0px;
    border-bottom: 2px solid #4258f4 !important;
    box-shadow: 0 1px 0 0 solid #4258f4 !important;
  }

  .select-lable {
    padding-bottom: 15px !important;
    font-size: 11px !important;
  }

  .md-form {
    margin-top: 1rem;
    margin-bottom: 0.25rem;
  }

  .nav {
    width: 100%;
  }

  .nav-tabs .nav-link.active {
    color: #f8f9fa;
    background-color: #4285f4;
    border-color: #dee2e6 #dee2e6 #4285f4;
  }

  .top-container {
    background-color: #f1f1f1;
    padding: 30px;
    text-align: center;
  }

  .header {
    padding: 10px 16px;
    background: #555;
    color: #f1f1f1;
  }

  .content {
    padding: 16px;
  }

  .sticky {
    position: fixed;
    top: 0;
    width: 100%;
  }

  .sticky+.content {
    padding-top: 102px;
  }
</style>

<div class="modal fade right" id="ShowGroupModal" tabindex="-1" role="dialog" data-keyboard="false"
  data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">

  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-fluid">
      {{--  <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Modal title</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>  --}}
      <div class=" md-tabs tabs-2 light-blue darken-3"> <span class="text-white"> </i>Group info </span> </div>
      <div class="modal-body">
        <h3 id="ShowGroupName" class="center mb-4"></h3>
     
        <div class="row">
          <div class="col-md-6">
            <div class="table-responsive">
              <div id="ShowUserGroupDiv"></div>
            </div>
          </div>
          <div class="col-md-6">
            <div id="ShowFileGroupDiv"></div>
          </div>
        </div>

      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="NewGroupModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static"
  aria-labelledby="exampleModalLabel" data-keyboard="false" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-fluid">
      <div class=" md-tabs tabs-2 light-blue darken-3"> <span class="text-white"> <i class="fas fa-plus mr-1"></i> New
          Group </span> </div>
      <div class="card-body">
        <div class="row">
          <!-- Second column -->
          <div class="col-lg-12 mb-4">
            <form id="NewGroupForm">
              <!-- First row -->
              @csrf

              <div class="col-md-12">
                <div class="md-form mb-0">
                  <input type="text" name="new_g_name" id="new_g_name" class="form-control validate">
                  <label for="new_g_name" class="active" data-error="wrong" data-success="right">Name</label>
                </div>
              </div>
              <div class="col-md-12 text-center my-4">
                <span class="waves-input-wrapper waves-effect waves-light"><input type="submit" value="Save"
                    class="btn btn-info btn-rounded"></span>
                <button type="button" class="btn btn-secondary btn-rounded btn-block1 my-4"
                  data-dismiss="modal">Close</button>
              </div>
              <!-- Fourth row -->
          </div>

          </form>
        </div>
        <!-- Second column -->

      </div>
    </div>
  </div>

</div>

<div class="modal fade" id="EditGroupModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static"
  aria-labelledby="exampleModalLabel" data-keyboard="false" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-fluid">
      <div class=" md-tabs tabs-2 light-blue darken-3"> <span class="text-white"> <i class="fas fa-edit mr-1"></i> Edit
          Group </span> </div>
      <div class="card-body">
        <div class="row">
          <!-- Second column -->
          <div class="col-lg-12 mb-4">
            <form id="EditGroupForm">
              <input type="hidden" name="edit_g_id" id="edit_g_id">
              <!-- First row -->
              @csrf

              <div class="col-md-12">
                <div class="md-form mb-0">
                  <input type="text" name="edit_g_name" id="edit_g_name" class="form-control validate">
                  <label for="edit_g_name" class="active" data-error="wrong" data-success="right">Name</label>
                </div>
              </div>
              <div class="col-md-12 text-center my-4">
                <span class="waves-input-wrapper waves-effect waves-light"><input type="submit" value="Save"
                    class="btn btn-info btn-rounded"></span>
                <button type="button" class="btn btn-secondary btn-rounded btn-block1 my-4"
                  data-dismiss="modal">Close</button>
              </div>
              <!-- Fourth row -->
          </div>

          </form>
        </div>
        <!-- Second column -->

      </div>
    </div>
  </div>

</div>

<div class="modal fade" id="NewRankModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static"
  aria-labelledby="exampleModalLabel" data-keyboard="false" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-fluid">
      <div class=" md-tabs tabs-2 light-blue darken-3"> <span class="text-white"> <i class="fas fa-plus mr-1"></i> New
          Rank </span> </div>
      <div class="card-body">
        <div class="row">
          <!-- Second column -->
          <div class="col-lg-12 mb-4">
            <form id="NewRankForm">
              <!-- First row -->
              @csrf

              <div class="col-md-12">
                <div class="md-form mb-0">
                  <input type="text" name="new_r_name" id="new_r_name" class="form-control validate">
                  <label for="new_r_name" class="active" data-error="wrong" data-success="right">Name</label>
                </div>
              </div>
              <div class="col-md-12 text-center my-4">
                <span class="waves-input-wrapper waves-effect waves-light"><input type="submit" value="Save"
                    class="btn btn-info btn-rounded"></span>
                <button type="button" class="btn btn-secondary btn-rounded btn-block1 my-4"
                  data-dismiss="modal">Close</button>
              </div>
              <!-- Fourth row -->
          </div>

          </form>
        </div>
        <!-- Second column -->

      </div>
    </div>
  </div>

</div>

<div class="modal fade" id="EditRankModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static"
  aria-labelledby="exampleModalLabel" data-keyboard="false" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-fluid">
      <div class=" md-tabs tabs-2 light-blue darken-3"> <span class="text-white"> <i class="fas fa-edit mr-1"></i> Edit
          Rank </span> </div>
      <div class="card-body">
        <div class="row">
          <!-- Second column -->
          <div class="col-lg-12 mb-4">
            <form id="EditRankForm">
              <input type="hidden" name="edit_r_id" id="edit_r_id">
              <!-- First row -->
              @csrf

              <div class="col-md-12">
                <div class="md-form mb-0">
                  <input type="text" name="edit_r_name" id="edit_r_name" class="form-control validate">
                  <label for="edit_r_name" class="active" data-error="wrong" data-success="right">Name</label>
                </div>
              </div>
              <div class="col-md-12 text-center my-4">
                <span class="waves-input-wrapper waves-effect waves-light"><input type="submit" value="Save"
                    class="btn btn-info btn-rounded"></span>
                <button type="button" class="btn btn-secondary btn-rounded btn-block1 my-4"
                  data-dismiss="modal">Close</button>
              </div>
              <!-- Fourth row -->
          </div>

          </form>
        </div>
        <!-- Second column -->

      </div>
    </div>
  </div>

</div>

<div class="modal fade right" id="ShowUserModal" tabindex="-1" role="dialog" data-keyboard="false"
  data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      {{--  <div class="modal-header">
      <h4 class="modal-title w-100" id="myModalLabel">Modal title</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>  --}}
      <div class=" md-tabs tabs-2 light-blue darken-3"> <span class="text-white"> </i>User info </span> </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <table>
              <tr>
                <td width="35%">Code</td>
                <td>
                  <strong class="text-primary" id="show_code"></strong>
                </td>
              </tr>
              <tr>
                <td>Name</td>
                <td>
                  <strong class="" id="show_name"></strong>
                </td>
              </tr>
              <tr>
                <td>Email</td>
                <td>
                  <strong class="text-info" id="show_email"></strong>
                </td>
              </tr>
              <tr>
                <td>Role</td>
                <td>
                  <strong class="" id="show_role"></strong>
                </td>
              </tr>
              <tr>
                <td>Rank</td>
                <td>
                  <strong class="" id="show_rank"></strong>
                </td>
              </tr>
              <tr>
                <td>Active</td>
                <td>
                  <strong class="" id="show_active"></strong>
                </td>
              </tr>

              <tr>
                <td>Authorized</td>
                <td>
                  <strong class="" id="show_authorized"></strong>
                </td>
              </tr>
              <tr>
                <td>Note</td>
                <td>
                  <strong class="" id="show_note"></strong>
                </td>
              </tr>
            </table>
          </div>
          <div class="col-md-6">
            <div id="ShowUserGroupsDiv"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<main class="mt-5 pt-5">
  <div class="container">
    <h4 class="text-center font-weight-bold mb-3"> Settings </h4>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link " id="tab00-tab" data-toggle="tab" href="#tab00" role="tab" aria-controls="tab00"
          aria-selected="true">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " id="tab01-tab" data-toggle="tab" href="#tab01" role="tab" aria-controls="tab01"
          aria-selected="true">Groups</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" id="tab02-tab" data-toggle="tab" href="#tab02" role="tab" aria-controls="tab02"
          aria-selected="false"> Groups & Users </a>
      </li>
      <li class="nav-item">
        <a class="nav-link " id="tab03-tab" data-toggle="tab" href="#tab03" role="tab" aria-controls="tab03"
          aria-selected="false"> File Permissions</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="tab04-tab" data-toggle="tab" href="#tab04" role="tab" aria-controls="tab04"
          aria-selected="false"> Ranks </a>
      </li>

    </ul>
    <div class="tab-content mt-3" id="myTabContent">
      <div class="tab-pane fade " id="tab00" role="tabpanel" aria-labelledby="tab00-tab">
        @include('include.user')
      </div>

      <div class="tab-pane fade " id="tab01" role="tabpanel" aria-labelledby="tab01-tab">
        <div class="col-md-12">
          <div class="form-group">
            <button type="button" class="btn btn-sm1 btn-primary" data-toggle="modal" data-target="#NewGroupModal">New
              Group</button>
          </div>
          <div class="table-responsive">
            <table class="table table-hover table-sm " style="font-size: 14px;" id="Groups" width="100%"
              cellspacing="0">
              <thead>
                <tr>
                  <th width="70%"> Name </th>
                  <th> Action </th>
                </tr>

              </thead>
              <tbody>
              </tbody>
            </table>
          </div>

        </div>
      </div>

      <div class="tab-pane fade show active " id="tab02" role="tabpanel" aria-labelledby="tab02-tab">
        <div class="col-md-12">
          <div class="form-group" style="height: 10px;">
            <h5 style="display: none; height: 100px;" class="UsersGroupLoader "><i
                class="fas fa-spinner fa-lg1 fa-spin fa-fw"></i>
              Please Wait .. </h5>
          </div>
          <div id="UsersGroup"></div>
        </div>
      </div>

      <div class="tab-pane fade " id="tab03" role="tabpanel" aria-labelledby="tab03-tab">
        <div class="col-md-12">
          <div class="form-group" style="height: 10px;">
            <h5 style="display: none; height: 100px;" class="FilePermissionsLoader "><i
                class="fas fa-spinner fa-lg1 fa-spin fa-fw"></i>
              Please Wait .. </h5>
          </div>
          <div id="FilePermissions"></div>
        </div>
      </div>

      <div class="tab-pane fade" id="tab04" role="tabpanel" aria-labelledby="tab04-tab">
        <div class="col-md-12">
          <div class="form-group">
            <button type="button" class="btn btn-sm1 btn-primary" data-toggle="modal" data-target="#NewRankModal">New
              Rank</button>
          </div>
          <div class="table-responsive">
            <table class="table table-hover table-sm " style="font-size: 14px;" id="Ranks" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th width="70%"> UserName </th>
                  <th> Action </th>
                </tr>

              </thead>
              <tbody>
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>

  </div>
</main>


<script>
  var Users;
  var GloalGroups = [];
  var header;
  var sticky;
  


      $(document).ready(function() {
          GroupsAndUsers();
          FilePermissions();
          {{-- ShowGroup(18); --}}

          Groups = $('#Groups').DataTable({
              orderCellsTop: true,
              responsive: !0,
              processing: true,
              serverSide: false,
              ajax: {
                  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                  type: 'post',
                  url :'/GetGroups',
                     
              },
              columns: [
                  {
                      data: function (data) {
                        {{-- console.log(data); --}}
                          return '<strong>' + data.name + '</strong>';
                      },
                      name: 'name'
                  },
                  {
                      data: function (data) {
                      str = '';
                          str+= `
                              <i onclick="ShowGroup(` + data.id + `) " data-toggle="tooltip" data-placement="top" title="Show Group" class="fa fa-eye text-secondary pointer"> </i> <span style="padding-right:10px"></span>
                              <i onclick="EditGroup(` + data.id + `) " data-toggle="tooltip" data-placement="top" title="Edit Group" class="fa fa-edit text-secondary pointer"> </i> <span style="padding-right:10px"></span>
                              <i onclick="DeleteGroup(` + data.id + `, '`  + data.name + `')" data-toggle="tooltip" data-placement="top" title="Delete Group" class="fa fa-trash-o text-secondary pointer"> </i> <span style="padding-right:10px"></span>
                              <i style="display: none" class="GroupLoader`+ data.id +` fas fa-spinner fa-lg1 text-success fa-spin fa-fw"></i>`;

                              return str;
                      },
                      name: 'id'
                  },
              ],
          });

          Ranks = $('#Ranks').DataTable({
              orderCellsTop: true,
              responsive: !0,
              processing: true,
              serverSide: false,
              ajax: {
                  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                  type: 'post',
                  url :'/GetRanks',
              },
              columns: [
                  {
                      data: function (data) {
                          return '<strong>' + data.name + '</strong>';
                      },
                      name: 'name'
                  },
                  {
                      data: function (data) {
                      str = '';
                          str+= `
                              <i onclick="EditRank(` + data.id + `) " data-toggle="tooltip" data-placement="top" title="Edit Rank" class="fa fa-edit text-secondary pointer"> </i> <span style="padding-right:10px"></span>
                              <i onclick="DeleteRank(` + data.id + `, '`  + data.name + `')" data-toggle="tooltip" data-placement="top" title="Delete Rank" class="fa fa-trash-o text-secondary pointer"> </i> <span style="padding-right:10px"></span>
                              <i style="display: none" class="RankLoader`+ data.id +` fas fa-spinner fa-lg1 text-success fa-spin fa-fw"></i>`;

                              return str;
                      },
                      name: 'id'
                  },
              ],
          });
          var table1 ;
      
      });

      $('#NewGroupForm').on('submit', function(e){
        e.preventDefault();
        var data = $(this).serializeArray();
        $.ajax({
            url: '/StoreGroup',
            data: data,
            type: 'post',
            success: function(data) {
                // console.log(data.status);
              if (data.status == 'success') {
                $('#Groups').DataTable().ajax.reload();
                $('#NewGroupModal').modal('hide');
                GroupsAndUsers();
                FilePermissions();
              } else {
                $('#NewGroupModal').modal('hide');
              }
            },
            complete: function() {},
        })
      });

      $('#EditGroupForm').on('submit', function(e){
        e.preventDefault();
        var data = $(this).serializeArray();
        // console.log(data)
        $.ajax({
            url: '/UpdateGroup',
            data: data,
            type: 'post',
            success: function(data) {
                // console.log(data.status);
            if (data.status == 'success') {
              $('#Groups').DataTable().ajax.reload();
                $('#EditGroupModal').modal('hide');
                GroupsAndUsers();
                FilePermissions();
            } else {
                $('#EditGroupModal').modal('hide');
            }
            },
            complete: function() {},
        })
    
      });
   
      $('#NewRankForm').on('submit', function(e){
        e.preventDefault();
        var data = $(this).serializeArray();
        $.ajax({
            url: '/StoreRank',
            data: data,
            type: 'post',
            success: function(data) {
                // console.log(data.status);
              if (data.status == 'success') {
                $('#Ranks').DataTable().ajax.reload();
                $('#NewRankModal').modal('hide');
               
              } else {
                $('#NewRankModal').modal('hide');
              }
            },
            complete: function() {},
        })
      });

      $('#EditRankForm').on('submit', function(e){
        e.preventDefault();
        var data = $(this).serializeArray();
        // console.log(data)
        $.ajax({
            url: '/UpdateRank',
            data: data,
            type: 'post',
            success: function(data) {
                // console.log(data.status);
            if (data.status == 'success') {
              $('#Ranks').DataTable().ajax.reload();
                $('#EditRankModal').modal('hide');
               
            } else {
                $('#EditRankModal').modal('hide');
            }
            },
            complete: function() {},
        })
    
      });

      $("#DeleteForm").on('submit',function(event){
        event.preventDefault();
          if ($('#deleteType').val() == 'Group'){
            $('.GroupLoader').fadeIn();
            url = '/DeleteGroup';
          }
          else {
            $('.RankLoader').fadeIn();
            url = '/DeleteRank';
          }
        
          data1 = $( this ).serializeArray();
          $.ajax({
              url: url,
              type: 'post',
              data: data1,
              success: function (data) {
                console.log(data)
                if (data.status == 'success'){ 
                  $('#ErrorMessage').css('display', 'none');
                  $('#DeleteModal').modal('hide');
                  GroupsAndUsers();
                  FilePermissions();
                    
                  if ($('#deleteType').val() == 'Group'){
                    $('.GroupLoader').fadeOut();
                    $('#Groups').DataTable().ajax.reload();
                  }
                  else {
                    $('.RankLoader').fadeOut();
                    $('#Ranks').DataTable().ajax.reload();
                  }
                }
                else if (data.status == 'fail'){
                  $('#ErrorMessage').text('Sorry, something went wrong ');
                  if ($('#deleteType').val() == 'Group'){
                    $('#ErrorMessage').text(data.message);
                  }
                  $('#ErrorMessage').css('display', 'block');
                }

              },
              error : function(){
                $('#ErrorMessage').css('display', 'block');
              },
          });
      });

      function ShowGroup(id) {
        $('.GroupLoader'+id).fadeIn();
        $('#edit_g_id').val(id);
        // console.log(id);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "/ShowGroup/" + id,
            type: 'post',
            success: function(response) {
              if(response.status == 'success'){
                // console.log(response);
                $('#ShowGroupName').text(response.Group.name);
                str =``;
                str +=`
                  
                    <table class="table table-hover table-sm table-striped " style="font-size: 13px; " id="ShowUsersGroupTable" width="100%"
                      cellspacing="0">
                      <thead>
                        <tr>
                          <th width="5%"> # </th>
                          <th width="5%"> Code </th>
                          <th width="90%">UserName </th>
                        </tr>
                      </thead>
                      <tbody>`;
  
                        c = 1;
                        for( i = 0; i < response.Group.user.length; i++ )
                        {
                          str +=`
                          <tr>
                            <td class="text-secondary">` + c++  + `</td>
                            <th class="text-primary">` + (response.Group.user[i].code).toUpperCase()  + `</th>
                            <th class="text-dark">` + response.Group.user[i].name  + `</th>
                          </tr>`;
                        }
                        str += `
                      </tbody>
                    </table>
                  
                `;

                $('#ShowUserGroupDiv').html(str);
                $('#ShowUsersGroupTable').dataTable({
                  "scrollX": false,
                  "scrollY": "250px",
                  "scrollCollapse": true,
                  sort : true,
                  "bPaginate": false,
                  "bLengthChange": false,
                  "bFilter": true,
                  "bInfo": false,
                  "bAutoWidth": false,
                });

                $('#ShowGroupModal').on('shown.bs.modal', function () {
                  $($.fn.dataTable.tables(true)).DataTable()
                     .columns.adjust();
               });
  

                str2 =``;
                str2 +=`
                  <div class="table-responsive">
                    <table class="table table-hover table-sm table-striped " style="font-size: 13px; " id="ShowFilesGroupTable" width="100%"
                      cellspacing="0">
                      <thead>
                        <tr>
                          <th width="5%"> # </th>
                          <th width="95%" > FileName </th>
                        </tr>
                      </thead>
                      <tbody>`;
  
                        x = 1;
                        for( j = 0; j < response.Group.file.length; j++ )
                        {
                          str2 +=`
                          <tr>
                            <td class="text-secondary">` + x++  + `</td>
                            <th class="text-dark">` + response.Group.file[j].name  + `</th>
                          </tr>`;
                        }
                        str2 += `
                      </tbody>
                    </table>
                  </div>
                `;

                $('#ShowFileGroupDiv').html(str2);
                var table2 = $('#ShowFilesGroupTable').dataTable({
                  "scrollX": false,
                  "scrollY": "250px",
                  "scrollCollapse": true,
                  sort : true,
                  "bPaginate": false,
                  "bLengthChange": false,
                  "bFilter": true,
                  "bInfo": false,
                  "bAutoWidth": false,
                });
              }

              $('#ShowGroupModal').on('shown.bs.modal', function () {
                $($.fn.dataTable.tables(true)).DataTable()
                   .columns.adjust();
             });

           
              {{-- table2.columns.adjust(); --}}
            
            },
              complete: function() {
                $('.GroupLoader'+id).fadeOut();
                $('#ShowGroupModal').modal('show');
            }
        });
      }

      function EditGroup(id) {
        $('.GroupLoader'+id).fadeIn();
        $('#edit_g_id').val(id);
        // console.log(id);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "/ShowGroup/" + id,
            type: 'post',
            success: function(response) {
              if(response.status == 'success'){
                // console.log(response);
                $('#edit_g_name').val(response.Group.name).siblings('label').addClass('active');
              }
            
            },
              complete: function() {
                $('.GroupLoader'+id).fadeOut();
                $('#EditGroupModal').modal('show');
            }
        });
      }

      function DeleteGroup(id, name) {
        $('#ErrorMessage').text('');
        $('#deleteID').val(id);
        $('#deleteName').text(name);
        $('#deleteType').val('Group');
        $('#DeleteModal').modal('show');
      }

      function EditRank(id) {
        $('.RankLoader'+id).fadeIn();
        $('#edit_r_id').val(id);
        // console.log(id);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "/ShowRank/" + id,
            type: 'post',
            success: function(response) {
              if(response.status == 'success'){
                // console.log(response);
                $('#edit_r_name').val(response.Rank.name).siblings('label').addClass('active');
              }
            
            },
              complete: function() {
                $('.RankLoader'+id).fadeOut();
                $('#EditRankModal').modal('show');
            }
        });
      }

      function DeleteRank(id, name) {
        $('#ErrorMessage').text('');
        $('#deleteID').val(id);
        $('#deleteName').text(name);
        $('#deleteType').val('Rank');
        $('#DeleteModal').modal('show');
      }

      function GroupsAndUsers(){
        $('.UsersGroupLoader').fadeIn();
        $.ajax({
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          url: "/GroupsAndUsers",
          type: 'post',
          success: function(data) {
            if(data != null ){
              {{-- console.log(data); --}}
              str = ``;
              str += `
              <form id ="UserGropusForm">
                <button type="submit" id="UserGropusFormbtn" class="btn btn-primary">Save Changes</button>
                <div class="table-responsive">
                  <table class="table table-hover table-sm table-bordered table-striped " style="font-size: 13px; " id="UsersGroupTable" width="100%"
                    cellspacing="0">
                    <thead>
                      <tr>
                        <th width="5%"> Code </th>
                        <th width="20%"> Users / Groups </th>`;
                        for( i = 0; i < data.Groups.length; i++ )
                        {
                          str += `<th class="center bg-success-ligth text-success">` + data.Groups[i].name  + `</th>`
                        }
                        str += `
                      </tr>
                    </thead>
                    <tbody>`;

                      for( j = 0; j < data.Users.length; j++ )
                      {
                        str +=`<tr>
                              <input type="hidden" name="users[]" value="`+ data.Users[j].id +`"> `;
                        str += `<th class="bg-secondary-ligth text-primary">` + data.Users[j].code  + `</th>
                                <th class="bg-secondary-ligth pointer" onclick="ShowUser(` + data.Users[j].id + `) ">` + data.Users[j].name  + `</th>`;
                          for( i = 0; i < data.Groups.length; i++ )
                          {
                            if (data.Users[j].group.length > 0){
                              flag = CheckGroupPermissions(data.Users[j].group, data.Groups[i] );
                              str += `<th class="center"> `;
                              if (flag){
                                str += `<input type="checkbox" name="checkbox[`+ data.Users[j].id +`][]" value="`+ data.Groups[i].id +`" checked> `;
                              }
                              else {
                                str += `<input type="checkbox" name="checkbox[`+ data.Users[j].id +`][]" value="`+ data.Groups[i].id +`"> `
                              }
                              str += `</th>`; 
                            }
                            else
                              str += `<th class="center"><input type="checkbox" name="checkbox[`+ data.Users[j].id +`][]" value="`+ data.Groups[i].id +`">  
                              </th>`;
                          }
                          str +=`</tr>`;
                      }
                      str += `
        
                    </tbody>
                  </table>
                </div>
              </form>
              `;
           
              $('#UsersGroup').html(str);
              var table11 =  $('#UsersGroupTable');
              table11.DataTable({
                "scrollX": false,
                "scrollY": "350px",
                "scrollCollapse": true,
                "fixedColumns": false,
                "sort" : true,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
              });

              $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                   .columns.adjust();
              });
            
              $("#UserGropusForm").on('submit',function(event){
                event.preventDefault();
                data1 = $( this ).serializeArray();
                  {{--  console.log(data1);  --}}
                  data1 = $( this ).serializeArray();
                  $.ajax({
                      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                      url: 'StoreGroupsUsers',
                      type: 'post',
                      data: data1,
                      success: function (data) {
                        if (data.status == 'success'){ 
                          GroupsAndUsers();
                          ShowAlert('success', 'Data Updated successfully');
                        }
                        else {
                          GroupsAndUsers();
                          ShowAlert('danger', 'sorry, something went wrong');
                        }
                      },
                      error : function(){
                        GroupsAndUsers();
                        ShowAlert('danger', 'sorry, something went wrong');
                      },
                  });
              });

            }
          },
          complete: function() {
            $('.UsersGroupLoader').fadeOut();
          }
        });
      }

      function FilePermissions(){
        $('.FilePermissionsLoader').fadeIn();
        $.ajax({
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          url: "/GroupsAndFiles",
          type: 'post',
          success: function(data) {
            if(data != null ){
              {{-- console.log(data); --}}
              str = ``;
              str += `
                <form id ="FilePermissionsForm">
                  <button type="submit" id="FilePermissionsFormbtn" class="btn btn-primary">Save Changes</button>
                  <div class="table-responsive" id="myHeader">
                    <table class="table table-hover table-sm table-bordered table-striped " style="font-size: 13px;" id="FilePermissionsTable" width="100%"
                      cellspacing="0">
                      <thead>
                        <tr>
                          
                          <th width="40%"> Files / Groups </th>`;
                          for( i = 0; i < data.Groups.length; i++ )
                          {
                            str += `<th class="center bg-success-ligth text-success">` + data.Groups[i].name  + `</th>`
                          }
                          str += `
                        </tr>
                      </thead>
                      <tbody>`;

                        for( j = 0; j < data.Files.length; j++ )
                        {
                          str +=`<tr>
                            <input type="hidden" name="files[]" value="`+ data.Files[j].id +`"> `;
                          str += `<th class="bg-secondary-ligth text-danger">
                            <a class="text-primary" target="_blank" href="/file/`+ + data.Files[j].id +`">` + data.Files[j].title  + `</a></th>`
                            for( i = 0; i < data.Groups.length; i++ )
                            {
                              if (data.Files[j].group.length > 0){
                                flag = CheckGroupPermissions(data.Files[j].group, data.Groups[i] );
                                str += `<th class="center"> `;
                                if (flag)
                                  str += `
                                          <input type="checkbox" name="checkbox[`+ data.Files[j].id +`][]" value="`+ data.Groups[i].id +`" checked> `;
                                else 
                                  str += `<input type="checkbox" name="checkbox[`+ data.Files[j].id +`][]" value="`+ data.Groups[i].id +`"> `
                                str += `</th>`; 
                              }
                              else
                                str += `<th class="center"><input type="checkbox" name="checkbox[`+ data.Files[j].id +`][]" value="`+ data.Groups[i].id +`">  
                                </th>`;
                            }
                            str +=`</tr>`;
                        }
                        str += `
          
                      </tbody>
                    </table>
                  </div>
                </form>
              `;
           
              $('#FilePermissions').html(str);
              $('#FilePermissionsTable').dataTable({
                "scrollX": false,
                "scrollY": "350px",
                "scrollCollapse": true,
                sort : false,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                
              });

              // Adjust columns width after clicking on tab
              $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                   .columns.adjust();
              });

              $("#FilePermissionsForm").on('submit',function(event){
                event.preventDefault();
                data1 = $( this ).serializeArray();
                //console.log(data1);
                  $.ajax({
                      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                      url: 'StoreGroupsFiles',
                      type: 'post',
                      data: data1,
                      success: function (data) {
                        if (data.status == 'success'){ 
                          FilePermissions();
                          ShowAlert('success', 'Data Updated successfully');
                        }
                        else {
                          FilePermissions();
                          ShowAlert('danger', 'sorry, something went wrong');
                        }
                      },
                      error : function(){
                        FilePermissions();
                        ShowAlert('danger', 'sorry, something went wrong');
                      },
                  });
              });
            }
          },
            complete: function() {
              $('.FilePermissionsLoader').fadeOut();
          }
        });
      }
  
   
</script>
@endsection



{{-- 
   for( k = 0; k < data.Users[j].group.length; k++ )
        {
          console.log(data.Users[j].group);
          // console.log(data.Users[j].group[k].pivot.group_id);
          console.log(k);

          if (data.Users[j].group[k].pivot.group_id ==  data.Groups[i].id)
          {
            str += `<input value="` + data.Groups[i].id + `"checked type="checkbox" name=""> ` + data.Groups[i].name;
            return ;
          }
          else {
            str += `<input type="checkbox" name=""> `
            return true;
          }
        }
         --}}