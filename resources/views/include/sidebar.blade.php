
  <div class="treeview w-10 border1" id="sidebar_tree">
  </div>
  <script>
    <?php  $Sidebar = App\Category::Sidebar(); ?>
    sb = JSON.parse( `{!! json_encode($Sidebar) !!}`)
    sbHtml=` <ul class="mb-1 pl-3 pb-2 mt-5 pt-5">`;
   
    function sidebar_tree( item , index )
    {
      {{-- console.log(1) --}}

      if(item['child'].length != 0 )
      {   
          sbHtml +=` <li><i class="fas fa-angle-right rotate"></i><span><i class="fas fa-folder-open text-warning ic-w mx-1 "></i><strong class="text-dark"> `+item['name']+` </strong></span><ul class="nested active ">`
          for (let [key, value] of Object.entries(item['child'])) {
            sidebar_tree(value,key);
          }
          sbHtml +="</ul></li>";
      } 
      else
        sbHtml +=` <li><span><i class="fas fa-folder-open text-warning ic-w mx-1 "></i><a href="/Manuals/`+item['id']+`"><strong class="text-dark1"> `+item['name']+` </strong></a></span>`
    }
    
    $(document).ready(function() {
              
          for (let [key, value] of Object.entries(sb)) {
            sidebar_tree(value,key);

          }
          sbHtml+=`</ul> `;
          $('#sidebar_tree').html(sbHtml)


            $('.treeview').mdbTreeview();
          });
  </script>
  {{--  {{ $Sidebar }}  --}}