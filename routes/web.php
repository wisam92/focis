<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/cc', function () {
        \Artisan::call('cache:clear');
        // \Artisan::call('route:cache');
        \Artisan::call('config:clear');
        \Artisan::call('view:clear');
        return 'done';
    });

Route::group([ 'middleware' => ['auth:web' ] ] , function()
{
    Route::get('/', function () {
        return view('index');
    });

    Route::get('/Manuals', function () {
        $parint = "";
        $cat = 0;
        return view('Files.manuals',compact('parint',"cat"));
    });

    // CategoryController
        Route::get('/Manuals/{id}', 'CategoryController@show_id');     
        Route::get('/Sidebar', 'CategoryController@Sidebar');     
        Route::post('/ShowCategory', 'CategoryController@show');
        Route::post('/Category/{id}', 'CategoryController@GoToCategory');
        Route::post('/StoreCategory', 'CategoryController@store');
        Route::post('/EditCategory', 'CategoryController@update');
        Route::post('/DeleteCategory', 'CategoryController@destroy');

    // FilesController
        Route::get('/file/{id}', 'FilesController@index');
        Route::post('/GetFile/{id}', 'FilesController@show');
        
        Route::post('/uploadFiles/{id}', 'FilesController@store');
        Route::post('/EditFile', 'FilesController@update');
        Route::post('/DeleteFile', 'FilesController@destroy');
        Route::post('/LatestFiles', 'FilesController@LatestFiles');
        Route::post('/AllFilesForReading', 'FilesController@AllFilesForReading');
        Route::post('/Reminders', 'FilesController@Reminders');
        Route::get('/MarkFileAsRead', 'FilesController@MarkFileAsRead');
        Route::get('/ConfirmReading', 'FilesController@ConfirmReading');
        

    // UserController
        //Route::get('/mail', 'MailController@SendEmailToNewEmployee');
        Route::get('/SendWelcome', 'UserController@SendWelcomingEmailToAllUsers');
        Route::post('/GetUsers', 'UserController@GetUsers');
        Route::post('/ShowUser/{id}', 'UserController@show');
        Route::post('/StoreUser', 'UserController@store');
        Route::post('/UpdateUser', 'UserController@update');
        Route::post('/ActiveUser', 'UserController@Active');
        Route::post('/ResetPassword', 'UserController@ResetPassword1');
        Route::post('/ResetPassword2', 'UserController@ResetPassword2');
    
    // GroupController
        Route::post('/GetGroups', 'GroupController@GetGroup');
        Route::post('/ShowGroup/{id}', 'GroupController@show');
        Route::post('/StoreGroup', 'GroupController@store');
        Route::post('/UpdateGroup', 'GroupController@update');
        Route::post('/DeleteGroup', 'GroupController@destroy');

    // UserGroupController
        Route::post('/GroupsAndUsers', 'UserGroupController@GroupsAndUsers');
        Route::post('/StoreGroupsUsers', 'UserGroupController@store');
        
    // GroupFileController
        Route::post('/GroupsAndFiles', 'GroupFileController@GroupsAndFiles');
        Route::post('/FileGroups/{id}', 'GroupFileController@FileGroups');
        Route::post('/StoreGroupsFiles', 'GroupFileController@store');
        Route::post('/UpdateGroupsFile', 'GroupFileController@update');

    // RankController
        Route::post('/GetRanks', 'RankController@GetRank');
        Route::post('/ShowRank/{id}', 'RankController@show');
        Route::post('/StoreRank', 'RankController@store');
        Route::post('/UpdateRank', 'RankController@update');
        Route::post('/DeleteRank', 'RankController@destroy');

    // ReportsController
        Route::get('/Reports', 'ReportsController@index');

    // SettingsController
        Route::get('/Settings', 'SettingsController@index');
    
});